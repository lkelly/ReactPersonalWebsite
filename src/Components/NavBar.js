import React from 'react';

import {Navbar, Nav, NavItem} from 'react-bootstrap';


require('./Styles/NavBar.scss');

class NavBar extends React.Component {
  constructor(props){
  super(props);
  this.state={isSolid:false};
  this.barColor = this.barColor.bind(this)
}
barColor(){
       let {isSolid} = this.state
      //  window.scrollY > this.prev?
        window.scrollY > 0?
       !isSolid && this.setState({isSolid:true})
       :
       isSolid && this.setState({isSolid:false})

       //this.prev = window.scrollY;
    }
    componentDidMount(){
        window.addEventListener('scroll',this.barColor);
    }
    componentWillUnmount(){
         window.removeEventListener('scroll',this.barColor);
    }
    render() {

        let navTransparency=this.state.isSolid?"solid":"stuff"

        return (
            <Navbar className={navTransparency} collapseOnSelect fixedTop>
                <Navbar.Header>
                    <Navbar.Brand>
                        <div className="navbar-brand">Liam Kelly</div>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav className="nav-items">
                      <NavItem>Work</NavItem>
                      <NavItem>Testimonials</NavItem>
                      <NavItem>Profile</NavItem>

                    </Nav>
                </Navbar.Collapse>
            </Navbar>

        );
    }
}

export default NavBar;
