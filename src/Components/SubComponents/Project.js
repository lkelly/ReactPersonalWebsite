import React, {Component} from 'react';
import {Image} from 'react-bootstrap';
//styles
import './SubStyles/Project.scss';

class Project extends Component {
  propTypes: {
     photoPath:  React.PropTypes.string.isRequired,
  }
  render() {
    return (
      <a className="project">
        <figure className="project-figure">
          <Image className="project-image" src={require('../../../public/pics/linden_mockup.png')}  />
          <div className="project-shine">
            <div className="sub-shine"></div>
          </div>
          <figcaption className="project-caption"></figcaption>
        </figure>

      </a>
    )
  }
}

export default Project;
