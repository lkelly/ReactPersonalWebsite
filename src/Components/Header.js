import React, {Component} from 'react';

//styles
import './Styles/Header.scss';


class Header extends Component {
  render() {
    return (
      <section className="header">
        <div className="background">

          <h1 className="title-text">Hi, I'm Liam</h1>
          <h3 className="sub-text">Web Developer</h3>

        </div>
      </section>
    )
  }
}

export default Header;
