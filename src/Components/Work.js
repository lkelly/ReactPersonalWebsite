import React, {Component} from 'react';
import Project from './SubComponents/Project';
//styles
import './Styles/Work.scss';

class Work extends Component {
  render() {
    return (
      <section className="work">
        <h3 className="work-title">My Work</h3>
        <section className="projects">

          <div className="test">
            <Project />
            <Project />
          </div>
        </section>
      </section>
    )
  }
}

export default Work;
