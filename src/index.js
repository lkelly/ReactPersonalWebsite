import React from 'react';
import ReactDOM from 'react-dom';
import NavBar from './Components/NavBar';
import Header from './Components/Header';
import Work from './Components/Work';
import './index.css';

ReactDOM.render(
  <div>
    <NavBar />
  <Header />
  <Work />
</div>,
  document.getElementById('root')
);
